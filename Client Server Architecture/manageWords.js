var express = require('express');
var fs = require('fs');
var router = express.Router();

router.use(express.static('public3'));

var file = "./words.json";
var dictionary;

fs.readFile(file, "utf-8", function (error, data) {
    dictionary = JSON.parse(data.trim());

});



router.get('/add', function (req, response) {
 console.log("add word request was made.");
   
    var word = req.query.word;
    var definition = req.query.def;
    if (word in dictionary){
        response.status(400).send("word already exists"); 
        console.log("Response - word already exists.");
    }else{
        dictionary[word] = definition;
        
        fs.writeFileSync('./words.json', JSON.stringify(dictionary));   
        response.status(200).send("data added.");
        console.log("Response - word successfully added.");
    }
    
    

});


router.get('/edit', function (req, response) {
    console.log("edit word meaning was made.");
    var word = req.query.word;
    var definition = req.query.def;
    
    if (word in dictionary){
         dictionary[word] = definition;
         fs.writeFileSync('./words.json', JSON.stringify(dictionary));
         response.status(200).send("Data modified successfully edited");
         console.log("Response - word modified successfully. ");
    }else{
         response.status(400).send("Word not found on the dictionary");
         console.log("Response - word does not exist. ");
    }
            
    });
        

module.exports = router;