
var fs = require("fs");
var express = require('express');
var app = express();

//imports two router 
var words = require('./manageWords.js');
var accounts = require('./accounts.js');


//app.use('/home', express.static(__dirname + '/public'));


var dictionary;
fs.readFile("./words.json","utf8",function(err, data){
    dictionary = JSON.parse(data.trim());
});
    

app.get('/dictionary', function(request, response){
   
    console.log("search request made");

    var word = request.query.word;
    word = word.toUpperCase();

    if(dictionary[word] != undefined){
        var meaning = dictionary[word];
        response.status(200).send(meaning);  
        console.log("Response - word meaning sent");
    }else {
        response.status(400).send('Word not found');
        console.log("Response - word meaning not found");
    }
});


app.get('/suggestion', function(request, response){
    
    console.log('suggestion request was made.');
    let result = [];
    var word = request.query.word; 
    word = word.toUpperCase();
    

   
    var words = [];
    for(var Aword in dictionary){
        words.push(Aword);
        
    }

   //iterate the array of words
    let  no = 0;
    for(var i = 0; i < words.length; i++){
        var currentWord = words[i];
       
        if(currentWord.startsWith(word)){
          
           result[no] = currentWord;
           no += 1;
        
        
            if(result >= 5){
                break;
            }
        }
    }
    
    if(result.length != 0){
        response.status(200).send(result);
        console.log("Response - suggestion sent.");
    }else{
        response.status(400).send('Word not found');
        console.log("Response - word not found.");
    }
});

app.get("/home", function(req, res){
    res.status(200).sendFile(__dirname + "/public/index.html");
});
app.use('/words', words);
//app.use('/words', words);
//app.use('/accounts/registers', accounts);
app.use('/accounts', accounts);


app.get('*', function(req, res){
    res.send('Sorry, this is an invalid URL.');
    });



app.listen(3000);