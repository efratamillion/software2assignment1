var express = require('express');
var fs = require('fs');
//var app = express();
var router = express.Router();

router.use(express.static('public4'));

var file = "./users.json";
var accounts;

fs.readFile(file, "utf-8", function (error, data) {
    accounts = JSON.parse(data.trim());

});

router.get('/register', function (req, response) {
    console.log("register request was made.");
    var userName = req.query.name;
    var passWord = req.query.password;

    if(userName in accounts){
        response.status(400).send("username already exists.please choose another userName: ");
        console.log("Reponse - username exists.");
    }else{
        accounts[userName] = passWord;
        fs.writeFileSync(file, JSON.stringify(accounts));       
        response.status(200).send("you have registered successfully!");
        console.log("Resopnse - registered successfully.");
    }
});
        
router.get('/login', function (request, response) {
    console.log("login request was made.");
    var userName = request.query.name;
    var password = request.query.password;
    
    if(userName in accounts){
        if(accounts[userName] == password){
            response.status(200).send("successfully logged in ");
            console.log('Reponse - successfully logged in.');
        }else{
            response.status(400).send("Incorrect userName or password");
            console.log("Response - incorrect passWord or username.");
        }
    }else{
        response.status(404).send("There is no account with that account.Please signup first.");
        console.log("Response - account does not exist.");
    }
});

module.exports = router;